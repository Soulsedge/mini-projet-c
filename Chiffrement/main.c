#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>
#include "AffichageResultat.h"
#include "GestionChar.h"
#include "Menu.h"
#include "ModificationVigenere.h"
#include "ModificationCesar.h"
#include "Demandeutilisateur.h"

void main()
{
	struct lconv *loc;
	setlocale(LC_ALL, "");
	loc = localeconv();

	//Demande choix algorithme & choix Chiffrement/Déchiffrement
	int ChoixAlgo;

	int ChoixAction;

	int ChoixSource;
	//L'alphabet tout simplement
	wchar_t Alphabet[26] = {L"abcdefghijklmnopqrstuvwxyz"};

	//Message à chiffrer
	wchar_t ch[1000];

	//Valeur de décalage
	int ValeurDecalage;

	//Clé pour vigenère
	wchar_t Cle[10];

	//Affichage menu de sélection
	MenuChoixAlgo(&ChoixAlgo, &ChoixAction, &ChoixSource);

	int DetectionErreur = 100;

	//Choix Selon la source entre la saisie clavier ou depuis document
	if (ChoixSource == 0) {
		DetectionErreur = GestionDocument(ch,Cle,ValeurDecalage,ChoixAction,ChoixAlgo,Alphabet);
	} else {
		DetectionErreur = GestionPhraseSaisie(ch,Cle,ValeurDecalage,ChoixAction,ChoixAlgo,Alphabet);
	}		

	AffichageErreur(DetectionErreur);
	
}
