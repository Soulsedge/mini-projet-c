int EstEspace (wchar_t valeur);
int VerificationChar(wchar_t* ptrch);
void ChangementChar(wchar_t *ptrch);
void GestionDocument (wchar_t *ch,wchar_t *Cle,int ValeurDecalage,int ChoixAction,int ChoixAlgo,wchar_t *Alphabet);
void GestionPhraseSaisie (wchar_t *ch,wchar_t *Cle,int ValeurDecalage,int ChoixAction,int ChoixAlgo,wchar_t *Alphabet);
