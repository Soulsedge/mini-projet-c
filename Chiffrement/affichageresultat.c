#include <stddef.h>
#include <stdio.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>
#include "gestionchar.h"

void EnregistrementLigne (FILE * fp,wchar_t *ch) {

	if (fp)
	{
		fprintf(fp,"%ls\n",ch);
	}
	else
	{
		wprintf(L"Erreur à l'ouverture du fichier\n");
	}

}

void AffichageResultat(wchar_t *ch)
{
	wprintf(L"Voici le résultat : %ls\n", ch);
}

void AffichageErreur (int valeur) {
	switch (valeur) {
		case 1:
			wprintf(L"Erreur ouverture document source\n");
			break;
		case 2:
			wprintf(L"Erreur ouverture document résultat\n");
			break;
		case 3:
			wprintf(L"Présence de caractère invalide\n");
			break;
		default:
			break;
	}
}