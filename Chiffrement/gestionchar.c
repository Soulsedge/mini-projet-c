#include <stddef.h>
#include <stdio.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>
#include "gestionchar.h"
#include "ModificationVigenere.h"
#include "ModificationCesar.h"
#include "Demandeutilisateur.h"
#include "AffichageResultat.h"

int EstEspace (wchar_t Valeur) {
	if (Valeur == ' ') {
		return 1;
	}
	return 0;
}

int VerificationChar(wchar_t* ptrch) 
{
	wchar_t CharNonValide[50] =  {L"!$<^>,/*()+-£&{}[]#?%0123456789!"};
    int compteur =0;
    int i =0;
	int taillech = wcslen(ptrch) ;
	if (taillech == 0) {
		taillech = 1;
	}
    while (i < taillech) {
		if (ptrch[i] == CharNonValide[compteur] ) {
			return 1;
        }
        compteur++;
        if (compteur == wcslen(CharNonValide) -1 ) {
			compteur =0;
            i++;
        }
    }
    return 0;		
}

void ChangementChar(wchar_t *ptrch)
{

	wchar_t CharAccentues [50] =	 {L"àÀâÂäÄéÉèÈêÊëËïÏîÎôÔöÖùÙûÛüÜÿ'ç"};
    wchar_t CharNonAccentues [50] =  {L"aaaaaaeeeeeeeeiiiioooouuuuuuy c"};
    
	int i = 0;
	while (i < wcslen(ptrch))
	{ 
		ptrch[i] = tolower(ptrch[i]);
		int compteur =0;
		while (compteur < wcslen(CharAccentues)) {
			if (ptrch[i] == CharAccentues[compteur]) {
				ptrch[i] = CharNonAccentues[compteur];
			}
			compteur++;
		}
		i++;
	}
}

int GestionDocument (wchar_t *ch,wchar_t *Cle,int ValeurDecalage,int ChoixAction,int ChoixAlgo,wchar_t *Alphabet) {

	// On vérifie l'ouverture des documents
	FILE *pf = NULL;
	pf = fopen("source.txt","r+");
	if (pf == NULL) {
		return 1;
	}
	FILE *resul = NULL;
	resul = fopen("resultat.txt","w+");
	if (resul == NULL) {
		return 2;
	}

	int x = 0;
	while (fgetws(ch,1000,pf) != NULL) {
		if (ch[wcslen(ch)-1] == '\n' ) {
			ch[wcslen(ch)-2] = '\0';
		}
		if (VerificationChar(ch) == 1) {
				return 3; //Signalement char interdit
		}
		ChangementChar(ch);
		if (ChoixAlgo == 0) {
			if (x == 0) {
				DemandeValeur(&ValeurDecalage);
			}
			ModificationCesar(ch, ChoixAction, ValeurDecalage, Alphabet);
		} else {
			if (x == 0) {
				DemanderCle(Cle);
			}
			ModificationVigenere(ch, ChoixAction, Cle, Alphabet);
		}
		EnregistrementLigne(resul,ch);
		x++;
	}	
		wprintf(L"Votre résultat a été enregistré dans le fichier resultat.txt !\n");
		fclose(pf);
		fclose(resul);
		return 0; //Tout est ok
}

int GestionPhraseSaisie (wchar_t *ch,wchar_t *Cle,int ValeurDecalage,int ChoixAction,int ChoixAlgo,wchar_t *Alphabet) {
	DemandeMessage(ch);
	if ( VerificationChar(ch) == 1) {
		return 3; //Signalement char interdit
	}
	ChangementChar(ch);
	if (ChoixAlgo == 0) {
		DemandeValeur(&ValeurDecalage);
		ModificationCesar(ch, ChoixAction, ValeurDecalage, Alphabet);
	} else {	
		DemanderCle(Cle);
		ModificationVigenere(ch, ChoixAction, Cle, Alphabet);
	}
	AffichageResultat(ch);
	return 0; // Tout est ok
	
}